
## Provided are the notebooks used for my bachelor thesis
The data used was preprocessed as such:
## Pre-processing in Matlab
General things about the data:
- There are 8 channels: {'Fpz', 'T7', 'O1', 'POz', 'Oz', 'Iz', 'O2', 'T8'}
- Trial time is 31.5 seconds
- sampling frequency is 120 
- 30 subjects with each having 5 blocks

A few pre-processing steps have already been done with the help of the jt_read_offline_data_gdf.m. For this script to work you need a few toolboxes:
1. FieldTrip from the Donders Institute
2. Signal Processing Toolbox from MathWorks

I will shortly summarize the pre-processing that has been done
1. Low-pass filter of 30 Hz applied
2. High-pass filter of 2 Hz applied
3. Downsampled
